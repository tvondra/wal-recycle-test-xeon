#!/bin/bash

while /bin/true; do
	date
	lvcreate -L 60G --snapshot --name test /dev/data-pvg/data

	sleep 60

	date
	lvremove -f /dev/data-pvg/test
done
