#!/bin/bash

LABEL=$1
DURATION=$((6*3600))


# 1500MB 7500MB 60GB
for s in 200 2000 8000; do

	mkdir $LABEL-$s

	pg_ctl -D /mnt/data/master -l $LABEL-$s/pg.log -w restart > $LABEL-$s/restart.log 2>&1

	dropdb --if-exists test > $LABEL-$s/init.log 2>&1
	createdb test >> $LABEL-$s/init.log 2>&1

	pgbench -i -s $s test >> $LABEL-$s/init.log 2>&1

	psql test -c "VACUUM ANALYZE" >> $LABEL-$s/init.log 2>&1
	psql test -c "CHECKPOINT" >> $LABEL-$s/init.log 2>&1

	pgbench -n -N -j 4 -c 16 -T $DURATION -l --aggregate-interval=1 test > $LABEL-$s/pgbench.log 2>&1

	mv pgbench_log.* $LABEL-$s

done
