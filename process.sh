#!/bin/bash

# CREATE TABLE pgbench_log (interval_start bigint, num_transactions bigint, sum_latency bigint, sum_latency_2 bigint, min_latency bigint, max_latency bigint);

for d in btrfs-master btrfs-no-recycle ext4-delalloc-master ext4-nodelalloc-master ext4-delalloc-no-recycle ext4-nodelalloc-no-recycle lvm-ext4-master lvm-ext4-no-recycle xfs-master xfs-no-recycle zfs-master zfs-no-recycle zfs-master-sata zfs-no-recycle-sata ext4-master-sata ext4-no-recycle-sata; do

	for s in 200 2000 8000; do

		psql test -c "truncate pgbench_log"

		for f in $d/$d-$s/pgbench_log.*; do
			cat $d/$d-$s/pgbench_log.* | psql test -c "copy pgbench_log from stdin with (delimiter ' ')"
		done

		psql test -c "select (interval_start - (select min(interval_start) from pgbench_log)) AS interval_start, sum(num_transactions) AS num_transactions, sum(sum_latency) as sum_latency, sum(sum_latency_2) AS sum_latency_2, min(min_latency) AS min_latency, max(max_latency) AS max_latency from pgbench_log group by interval_start order by interval_start" > $d/$d-$s.csv

		psql test -c "with src as (select (interval_start - (select min(interval_start) from pgbench_log)) AS interval_start, sum(num_transactions) AS num_transactions, sum(sum_latency) as sum_latency, sum(sum_latency_2) AS sum_latency_2, min(min_latency) AS min_latency, max(max_latency) AS max_latency from pgbench_log group by interval_start order by interval_start)
		select (interval_start/60) as interval_start, avg(num_transactions)::int as avg_transactions, sum(num_transactions) AS num_transactions, sum(sum_latency) as sum_latency, sum(sum_latency_2) AS sum_latency_2, min(min_latency) AS min_latency, max(max_latency) AS max_latency from src group by interval_start/60 order by interval_start/60" > $d/$d-$s-aggregate.csv

	done

done


for d in lvm-ext4-master-snapshots lvm-ext4-no-recycle-snapshots zfs-master-2 zfs-no-recycle-2; do

	for s in 200 2000 6000; do

		psql test -c "truncate pgbench_log"

		for f in $d/$d-$s/pgbench_log.*; do
			cat $d/$d-$s/pgbench_log.* | psql test -c "copy pgbench_log from stdin with (delimiter ' ')"
		done

		psql test -c "select (interval_start - (select min(interval_start) from pgbench_log)) AS interval_start, sum(num_transactions) AS num_transactions, sum(sum_latency) as sum_latency, sum(sum_latency_2) AS sum_latency_2, min(min_latency) AS min_latency, max(max_latency) AS max_latency from pgbench_log group by interval_start order by interval_start" > $d/$d-$s.csv

		psql test -c "with src as (select (interval_start - (select min(interval_start) from pgbench_log)) AS interval_start, sum(num_transactions) AS num_transactions, sum(sum_latency) as sum_latency, sum(sum_latency_2) AS sum_latency_2, min(min_latency) AS min_latency, max(max_latency) AS max_latency from pgbench_log group by interval_start order by interval_start)
		select (interval_start/60) as interval_start, avg(num_transactions)::int as avg_transactions, sum(num_transactions) AS num_transactions, sum(sum_latency) as sum_latency, sum(sum_latency_2) AS sum_latency_2, min(min_latency) AS min_latency, max(max_latency) AS max_latency from src group by interval_start/60 order by interval_start/60" > $d/$d-$s-aggregate.csv

	done

done
